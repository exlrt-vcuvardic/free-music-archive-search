export const sliderTempoConfig = {
	selector: '#slider-tempo',
	options: {
		ticks: [1, 2, 3, 4, 5, 6],
		ticks_labels: ['All', 'Very Slow', 'Slow', 'Medium', 'Fast', 'Very Fast'],
		min: 1,
		max: 6,
		step: 1,
		value: 1,
		tooltip: 'hide'
	}
};

export const sliderDurationConfig = {
	selector: '#slider-duration',
	options: {
		ticks: [0, 1, 2, 3, 4, 5, 6],
		ticks_labels: ['0:00', '1:00', '2:00', '3:00', '4:00', '5:00', '6:00+'],
		min: 0,
		max: 6,
		step: 1,
		value: [0, 6],
		focus: true,
		tooltip: 'hide'
	}
};
