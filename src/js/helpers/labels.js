export const licenceLabels = {
	'music-filter-CC-attribution-only': 'CC BY',
	'music-filter-CC-attribution-sharealike': 'CC BY SA',
	'music-filter-CC-attribution-noderivatives': 'CC BY ND',
	'music-filter-CC-attribution-noncommercial': 'CC BY NC',
	'music-filter-CC-attribution-noncommercial-sharealike': 'CC BY NC SA',
	'music-filter-CC-attribution-noncommercial-noderivatives': 'CC BY NC ND',
	'music-filter-public-domain': 'Public Domain',
	'music-filter-commercial-allowed': 'Commercial Use',
	'music-filter-remix-allowed': 'Use in a Remix or Video'
};

export const tempoLabels = ['All', 'Very Slow', 'Slow', 'Medium', 'Fast', 'Very Fast'];
