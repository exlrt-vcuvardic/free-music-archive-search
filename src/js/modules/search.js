import * as Slider from '../../../node_modules/bootstrap-slider/src/js/bootstrap-slider';
import { bookmark } from './bookmark';
import { licenceLabels, tempoLabels } from '../helpers/labels';
import { sliderTempoConfig, sliderDurationConfig } from '../helpers/slider-config';

export class Search {
	constructor() {
		this.searchURI = 'http://138.197.106.55/index.php/search';
		this.filterValues = {
			adv: null,
			quicksearch: null,
			'search-genre': null,
			'only-instrumental': null,
			duration_from: 0,
			duration_to: 6,
			tempo: null,
			'music-filter-CC': null,
			'music-filter-CC-attribution-only': null,
			'music-filter-CC-attribution-sharealike': null,
			'music-filter-CC-attribution-noderivatives': null,
			'music-filter-CC-attribution-noncommercial': null,
			'music-filter-CC-attribution-noncommercial-sharealike': null,
			'music-filter-CC-attribution-noncommercial-noderivatives': null,
			'music-filter-public-domain': null,
			'music-filter-commercial-allowed': null,
			'music-filter-remix-allowed': null
		};

		this.initSearchSliders();
		this.addEventListeners();

		if (window.location.search.length) {
			this.setFilterValuesFromUriParams();
		}
	}

	initSearchSliders() {
		this.sliderTempo = new Slider(sliderTempoConfig.selector, sliderTempoConfig.options);
		this.sliderDuration = new Slider(sliderDurationConfig.selector, sliderDurationConfig.options);
	}

	refreshSliders() {
		this.sliderTempo.refresh();
		this.sliderDuration.refresh();
		if (this.filterValues.tempo) {
			this.sliderTempo.setValue(tempoLabels.indexOf(this.filterValues.tempo) + 1);
		}
		if (!(this.filterValues.duration_from === 0 && this.filterValues.duration_to === 6)) {
			this.sliderDuration.setValue([this.filterValues.duration_from, this.filterValues.duration_to]);
		}
	}

	closeSearchFilters() {
		$('.js-dropdown').removeClass('dropdown--is-active');
	}

	toggleActiveLabel(name, category, toggle, customText) {
		$(`.js-label[data-name="${name}"`).remove();
		if (toggle) {
			$('.js-filter-labels').append(`
				<div class="js-label filter-label" data-name="${name}">
					${customText ? customText.toUpperCase() : name.toUpperCase()}
					<i
						class="fa fa-times filter-label-close js-removeFilter"
						data-filter-name=${name}
						data-filter-category=${category}
					></i>
				</div>
			`)
		}
		$('.js-labels-controls').toggleClass('hide', !$('.js-filter-labels').children().length);
	}

	setDurationValue(from, to) {
		this.filterValues.duration_from = from;
		this.filterValues.duration_to = to;
		this.toggleActiveLabel('duration', 'duration', false);
		if (from === 0 && to === 6) {
			return;
		}
		this.toggleActiveLabel('duration', 'duration', true,
			`Duration ${from}:00 - ${to}:00${to === 6 ? '+' : ''}`);
	}

	setTempoValue(name) {
		this.filterValues.tempo = name === 'All' ? null : name;
		this.toggleActiveLabel('tempo', 'tempo', name !== 'All', name);
	}

	addSliderEvents() {
		this.sliderDuration.on('slideStop', duration => {
			this.setDurationValue(duration[0], duration[1]);
		});
		this.sliderTempo.on('slideStop', tempoIndex => {
			this.setTempoValue(tempoIndex !== 1 ? tempoLabels[tempoIndex - 1] : null);
		});
	}

	activateDropdowns() {
		$.each($('.js-dropdown-toggle'), (i, btn) => {
			const $btn = $(btn);
			$btn.click(() => {
				this.closeSearchFilters();
				$btn.parents('.js-dropdown').addClass('dropdown--is-active');
				this.refreshSliders();
				this.addSliderEvents();
			});
		});
		$('.js-close-dropdowns').click(() => this.closeSearchFilters());
	}

	toggleInstrumental(toggle) {
		this.filterValues['only-instrumental'] = toggle ? 1 : null;
		$('.js-instrumental-switch').toggleClass('header-search-switch--is-active', toggle);
		this.toggleActiveLabel('only-instrumental', 'only-instrumental', toggle, 'INSTRUMENTAL');
	}

	activateInstrumentalSwitch() {
		$('.js-instrumental-switch').click(event => {
			this.toggleInstrumental(!$(event.target).hasClass('header-search-switch--is-active'));
		});
	}

	saveSearchTerm() {
		const searchVal = $('#main-search').val();
		this.filterValues.quicksearch = searchVal === '' ? null : searchVal;
	}

	generateParams() {
		this.saveSearchTerm();
		let params = '';
		for (const filter in this.filterValues) {
			if (this.filterValues[filter] !== null) {
				if (params !== '') {
					params += '&';
				}
				if (!((filter === 'duration_from' || filter === 'duration_to') &&
				(this.filterValues.duration_from === 0 && this.filterValues.duration_to === 6))) {
					params += `${filter}=${this.filterValues[filter]}`;
				}
			}
		}
		return params !== '' ? `${this.searchURI}?adv=1&${params}` : this.searchURI;
	}

	activateExecuteSearch() {
		$('.js-search-execute').click(() => window.location.assign(this.generateParams()));
	}

	activateFilter(id, show) {
		const $input = $(`#${id}`);
		this.filterValues[id] = show ? 1 : null;
		$input[0].checked = show;

		$input.parent()
			.toggleClass('f_unchecked', !show)
			.toggleClass('f_checked', show);

		this.toggleActiveLabel(id, 'license-CC', show, licenceLabels[id]);
	}

	disableFilter(id, disable) {
		$(`#${id}`)
			.attr('disabled', disable ? 'disable' : false)
			.parent()
				.toggleClass('f_disabled', disable);
	}

	activateLicenseUsageFilter() {
		$('input[name="license-type"]').change(event => {
			const activate = !this.filterValues[event.target.id];
			if (event.target.id === 'music-filter-commercial-allowed') {
				this.activateFilter('music-filter-public-domain', activate);
				this.activateFilter('music-filter-CC-attribution-only', activate);
				this.activateFilter('music-filter-CC-attribution-sharealike', activate);
				this.activateFilter('music-filter-CC-attribution-noderivatives', activate);
				this.disableFilter('music-filter-CC-attribution-noncommercial', activate);
				this.disableFilter('music-filter-CC-attribution-noncommercial-sharealike', activate);
				this.disableFilter('music-filter-CC-attribution-noncommercial-noderivatives', activate);
				if (this.filterValues['music-filter-remix-allowed']) {
					if (activate) {
						this.activateFilter('music-filter-CC-attribution-noderivatives', false);
						this.activateFilter('music-filter-CC-attribution-noncommercial-sharealike', false);
						this.activateFilter('music-filter-CC-attribution-noncommercial-noderivatives', false);
						this.activateFilter('music-filter-CC-attribution-noncommercial', false);
					} else {
						this.activateFilter('music-filter-public-domain', true);
						this.activateFilter('music-filter-CC-attribution-only', true);
						this.activateFilter('music-filter-CC-attribution-sharealike', true);
						this.activateFilter('music-filter-CC-attribution-noncommercial', true);
						this.activateFilter('music-filter-CC-attribution-noncommercial-sharealike', true);
						this.disableFilter('music-filter-CC-attribution-noncommercial-noderivatives', true);
					}
				}
			}
			if (event.target.id === 'music-filter-remix-allowed') {
				this.activateFilter('music-filter-public-domain', activate);
				this.activateFilter('music-filter-CC-attribution-only', activate);
				this.activateFilter('music-filter-CC-attribution-sharealike', activate);
				this.activateFilter('music-filter-CC-attribution-noncommercial', activate);
				this.activateFilter('music-filter-CC-attribution-noncommercial-sharealike', activate);
				this.disableFilter('music-filter-CC-attribution-noderivatives', activate);
				this.disableFilter('music-filter-CC-attribution-noncommercial-noderivatives', activate);

				if (this.filterValues['music-filter-commercial-allowed']) {
					this.activateFilter('music-filter-public-domain', true);
					this.activateFilter('music-filter-CC-attribution-only', true);
					this.activateFilter('music-filter-CC-attribution-sharealike', true);
					this.activateFilter('music-filter-CC-attribution-noderivatives', !activate);
					if (activate) {
						this.activateFilter('music-filter-CC-attribution-noncommercial', false);
						this.activateFilter('music-filter-CC-attribution-noncommercial-sharealike', false);
						this.activateFilter('music-filter-CC-attribution-noncommercial-noderivatives', false);
					} else {
						this.disableFilter('music-filter-CC-attribution-noncommercial', true);
						this.disableFilter('music-filter-CC-attribution-noncommercial-sharealike', true);
						this.disableFilter('music-filter-CC-attribution-noncommercial-noderivatives', true);
					}
				}
			}

			this.filterValues[event.target.id] = this.filterValues[event.target.id] ? null : 1;

			this.toggleActiveLabel(
				event.target.id,
				'license-usage',
				this.filterValues[event.target.id] !== null,
				licenceLabels[event.target.id]
			);
		});
	}

	activateLicenseCC() {
		$('#license-cc input').change(event => {
			this.filterValues[event.target.id] = event.target.checked ? true : null;
			this.toggleActiveLabel(
				event.target.id,
				'license-CC',
				event.target.checked,
				licenceLabels[event.target.id]);
		});
	}

	displayGenreFilters() {
		$('.js-header-search-genre-input')
			.val(this.filterValues['search-genre'] ? this.filterValues['search-genre'].join(', ') : '');
	}

	toggleGenre(genre, toggle) {
		if (toggle) {
			this.filterValues['search-genre'] = this.filterValues['search-genre'] || [];
			this.filterValues['search-genre'].push(genre);
		} else {
			if (this.filterValues['search-genre'] && this.filterValues['search-genre'].length) {
				this.filterValues['search-genre'] = this.filterValues['search-genre']
					.filter(activeGenre => activeGenre !== genre);
				this.filterValues['search-genre'] = this.filterValues['search-genre'].length
					? this.filterValues['search-genre'] : null;
			} else {
				this.filterValues['search-genre'] = null;
			}
		}
		this.toggleActiveLabel(genre, 'genre', toggle);
		$(`.js-genre:contains(${genre})`).toggleClass('header-search-genre--is-selected', toggle);
		this.displayGenreFilters();
	}

	toggleAllGenres(toggle) {
		$.each($('.js-genre'), (index, genre) => {
			$(genre).toggleClass('header-search-genre--is-selected', toggle);
			this.toggleGenre(genre.innerText, toggle);
		});
	}

	activateGenreFilter() {
		$('.js-genre').click(event => {
			this.toggleGenre(event.target.innerText, !$(event.target)
				.hasClass('header-search-genre--is-selected'));
		});

		$('.js-toggle-all-genres').click(event => {
			this.toggleAllGenres($(event.target).hasClass('js-select-all-genres'));
		});
	}

	resetDurationSlider() {
		this.filterValues.duration_from = 0;
		this.filterValues.duration_to = 6;
		$('.js-label-duration').remove();
		this.toggleActiveLabel('duration', 'duration', false);
	}

	removeFilter(filter) {
		const $filter = $(filter);
		const name = $filter.data('filter-name');
		const category = $filter.data('filter-category');
		switch (category) {
			case 'genre':
				this.toggleGenre(name, false);
				break;
			case 'only-instrumental':
				this.toggleInstrumental(false);
				break;
			case 'duration':
				this.resetDurationSlider();
				break;
			case 'tempo':
				this.tempo = null;
				this.setTempoValue('All');
				break;
			case 'license-CC':
			case 'license-usage':
				this.filterValues[name] = null;
				this.activateFilter(name, false);
				break;
			default:
				break;
		}
	}

	activateLabelRemove() {
		$(document).on('click', '.js-removeFilter', event => {
			this.removeFilter(event.target);
			$(event.target).parent().remove();
		});
	}

	clearLicenseFilters() {
		$.each($('.js-license input'), (index, filter) => {
			this.activateFilter(filter.id, false);
			this.disableFilter(filter.id, false);
		});
	}

	onClearSearch() {
		this.toggleInstrumental(false);
		this.resetDurationSlider();
		this.setTempoValue('All');
		this.toggleAllGenres(false);
		this.clearLicenseFilters();
		$('#main-search').val('');
	}

	activateSearchControls() {
		$(document).on('click', '.js-bookmark-search', () => {
			bookmark();
		});

		$(document).on('click', '.js-clear-search', () => {
			this.onClearSearch();
		});
	}

	addGlobalClick() {
		$(document).click(event => {
			if (!$(event.target).parents('.js-dropdown').length) {
				this.closeSearchFilters();
			}
		});
	}

	addEventListeners() {
		this.activateDropdowns();
		this.activateInstrumentalSwitch();
		this.activateExecuteSearch();
		this.activateLicenseUsageFilter();
		this.activateLicenseCC();
		this.activateGenreFilter();
		this.activateLabelRemove();
		this.activateSearchControls();
		this.addGlobalClick();
	}

	setFilterValuesFromUriParams() {
		const searchParams = new URLSearchParams(window.location.search);
		let durationSet = false;
		for (const key of searchParams.keys()) {
			const value = searchParams.get(key);
			switch (key) {
				case 'quicksearch':
					$('#main-search').val(value);
					break;
				case 'search-genre':
					const activeGenres = value.split(',');
					activeGenres.forEach(genre => {
						this.toggleGenre(genre, true);
					});
					break;
				case 'only-instrumental':
					this.toggleInstrumental(value === '1');
					break;
				case 'duration_from':
				case 'duration_to':
					if (durationSet) {
						break;
					}
					const from = searchParams.get('duration_from') || 0;
					const to = searchParams.get('duration_to') || 6;
					this.setDurationValue(Number(from), Number(to));
					durationSet = true;
					break;
				case 'tempo':
					this.setTempoValue(value);
					this.sliderTempo.refresh();
					break;
				case 'music-filter-CC-attribution-only':
				case 'music-filter-CC-attribution-sharealike':
				case 'music-filter-CC-attribution-noderivatives':
				case 'music-filter-CC-attribution-noncommercial':
				case 'music-filter-CC-attribution-noncommercial-sharealike':
				case 'music-filter-CC-attribution-noncommercial-noderivatives':
				case 'music-filter-public-domain':
				case 'music-filter-remix-allowed':
				case 'music-filter-commercial-allowed':
					this.activateFilter(key, true);
					break;
				default:
					break;
			}
		}
	}
};
